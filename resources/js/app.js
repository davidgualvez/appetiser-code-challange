require('./bootstrap');
const moment = require('moment');
import Toasted from 'vue-toasted';

import axios from 'axios';
import Vue from 'vue'

Vue.use(Toasted);

var app = new Vue({
    el: '#app',
    data: {
        event_name: '',
        event_from: '',
        event_to: '',
        days_of_week: [
            {
                day_of_week: 1,
                desc: 'Mon',
                is_checked: false,
            },
            {
                day_of_week: 2,
                desc: 'Tue',
                is_checked: false,
            },
            {
                day_of_week: 3,
                desc: 'Wed',
                is_checked: false,
            },
            {
                day_of_week: 4,
                desc: 'Thu',
                is_checked: false,
            },
            {
                day_of_week: 5,
                desc: 'Fri',
                is_checked: false,
            },
            {
                day_of_week: 6,
                desc: 'Sat',
                is_checked: false,
            },
            {
                day_of_week: 0,
                desc: 'Sun',
                is_checked: false,
            },
        ],
        display: {
            collection: []
        }
    },
    methods: {
        onSaveClick(){

            var selected = false;
            this.days_of_week.forEach(element => {
                if(element.is_checked){
                    selected = true;
                }
            });

            if(!selected){
                this.showMsg('Please Select atleast One, day of the week', 'error');
                return;
            }


            axios.post(`/calendar-event`, {
                event_name: this.event_name,
                event_from: this.event_from,
                event_to: this.event_to,
                selected_days: this.days_of_week,
            }).then(res=>{
                this.showMsg(res.data.message);
                this.updateDisplay();

            }).catch(err=>{
                this.showMsg(err.response.data.message, 'error');
                // alert(err.response.data.message);
            });

        },
        updateDisplay(){
            var start = moment().startOf('month');
            var end = moment().endOf('month');
            this.display.collection = [];
            for(start; start < end; start.add(1, 'days')){

                var event = {
                    name: '',
                    is_match: false,
                };

                if(
                    start >= moment(this.event_from).startOf('day') &&
                    start <= moment(this.event_to).endOf('day')
                ){

                    this.days_of_week.forEach(element => {
                        if(element.day_of_week == start.clone().day() && element.is_checked){
                            event.is_match = true;
                            event.name = this.event_name;
                        }
                    });

                }

                this.display.collection.push({
                    day: start.format('D'),
                    month: start.format('MMM'),
                    year: start.format('YYYY'),
                    day_of_week: start.format('ddd'),
                    ...event
                });
            }
        },
        showMsg(msg, type = 'success'){

            if(type == 'error'){
                this.$toasted.error(msg, {
                    duration : 2000
                });
                return;
            }

            this.$toasted.success(msg, {
                duration : 2000
            });
        },
    },
    mounted(){
        this.updateDisplay();
    }
});
