<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Appetiser Code Challenge</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="flex justify-between sticky top-0 bg-black sm:bg-red-500 md:bg-green-500 lg:bg-blue-500 xl:bg-yellow-500 2xl:bg-indigo-500 text-white tracking-wider p-1 text-xs">
        <div>
            <div class="sm:hidden">Default</div>
            <div class="hidden sm:block md:hidden">sm: Mobile</div>
            <div class="hidden md:block lg:hidden">md: Tablet</div>
            <div class="hidden lg:block xl:hidden">lg: Desktop</div>
            <div class="hidden xl:block 2xl:hidden">xl: Wide Desktop</div>
            <div class="hidden 2xl:block">2xl: Ultra Wide Desktop</div>
        </div>
        <div>
            Appetiser Code Challenge
        </div>
        <div>
            Screen Guide
        </div>
    </div>

    <div id="app" class="bg-gray-200  p-4 ">
        <div class="card">
            <div class="card-header">Calendar</div>
            <div class="card-body">
                <div class="grid grid-cols-12 gap-3">
                    <div class="col-span-12 sm:col-span-4">
                        <div>
                            <label>Event</label>
                            <input v-model="event_name" type="text" class="block w-full border px-2 py-1" placeholder="">
                        </div>
                        <div class="gid grid-cols-12">
                            <div class="col-span-6  mt-2">
                                <div>
                                    <label>From</label>
                                    <input v-model="event_from" type="date" class="block w-full border px-2 py-1" placeholder="">
                                </div>
                            </div>
                            <div class="col-span-6  mt-2">
                                <div>
                                    <label>To</label>
                                    <input v-model="event_to" type="date" class="block w-full border px-2 py-1" placeholder=".">
                                </div>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label v-for="(dof, index) in days_of_week" :for="index">
                                <input v-model="dof.is_checked" :id="index" type="checkbox">
                                @{{ dof.desc }}
                            </label>
                        </div>
                        <div>
                            <button @click="onSaveClick" class="px-3 px-1 bg-blue-500 text-white mt-2">
                                Save
                            </button>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-8">
                        <div>

                            <ul>
                                <li v-for="item in display.collection"  class="">
                                    <div v-if="item.day == 1" class="border-b ">
                                        <h1 class="text-2xl font-medium">@{{ item.month }} @{{ item.year }}</h1>
                                    </div>
                                    <div class="grid grid-cols-12 border-b p-2" :class="{ 'bg-green-100':item.is_match }">
                                        <div class="col-span-4">@{{ item.day }} @{{ item.day_of_week }}</div>
                                        <div class="col-span-8">@{{ item.name }}</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
