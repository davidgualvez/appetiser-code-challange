<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_name',
        'event_from',
        'event_to',
    ];

    /**
     * Relationships
     */
    public function dayOfWeek(){
        return $this->hasMany(CalendarEventDate::class, 'calendar_event_id');
    }
}
