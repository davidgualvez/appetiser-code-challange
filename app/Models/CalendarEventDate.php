<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendarEventDate extends Model
{
    use HasFactory;

    protected $fillable = [
        'calendar_event_id',
        'day_of_week',
    ];
}
