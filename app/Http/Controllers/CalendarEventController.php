<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalendarEventRequest;
use App\Models\CalendarEvent;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CalendarEventController extends Controller
{
    public function page(Request $request){
        return view('app');
    }

    public function store(CalendarEventRequest $request){

        try{
            DB::beginTransaction();

            $from = Carbon::parse($request->event_from);
            $to = Carbon::parse($request->event_to);

            if($from > $to){
                DB::rollBack();
                return response()->json([
                    'message' => 'Event From must be lower than the Event To.'
                ], 400);
            }

            $ce = CalendarEvent::create($request->except('selected_days'));

            foreach( $request->selected_days as $key=>$item){
                if($item['is_checked']){
                    $ce->dayOfWeek()->create($item);
                }
            }

            DB::commit();
            return response()->json([
                'message' => 'Event has been save.'
            ]);
        }catch(Exception $e){
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }

    }
}
